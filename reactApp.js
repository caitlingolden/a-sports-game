//completed last step with help from Yaseen
//completed audio with assistance from Chip
//worked with my SQUAD (Steven, TL, Jamal, Elisia, Mai, Deidre) to get started
//copied and pasted code from GIT to avoid errors with repos
// Default App component that all other compents are rendered through
function App() {
    return (
        <div>
        <Game venue="SeaWorld" teamOne="Walruses" teamOneLogo="Walrus Cartoon_PREVIEW.jpg"
        teamTwo="Seals" teamTwoLogo="seal.jpg"/> 
        </div>
    )
}
class Game extends React.Component {
render(){
    return(
        <div>
                <h1>Welcome to {this.props.venue}!</h1>
            This file represents the code after completing the setup step in the lab instructions
                <Team name={this.props.teamOne} logo={this.props.teamOneLogo} />
                <Team name={this.props.teamTwo} logo={this.props.teamTwoLogo} />
                
                </div>
    )
}

}
class Team extends React.Component {
    state = {
        shotsTaken: 0,
        score: 0,
        // percentage: null
    }
    soundEffects = { 
        shooting: "BOUNCE+1.mp3",
        scoring : "Swish+2.mp3"
    }

    shots = () => {
        new Audio (this.soundEffects.shooting).play()
        this.setState((state) => {
            console.log("shots works")
            return { shotsTaken: state.shotsTaken + 1 }
            
        })

        let shot = Math.round(Math.random() * 10)
        if (shot % 3 === 0) {
            new Audio (this.soundEffects.scoring).play()
            console.log("scoring works")
            this.setState((state) => {
                return {score:state.score + 1}

       })
    }
}
    percentage = () => {
        // this.setState(()=> {
        if(this.state.shotsTaken > 0){
            return "Percentage:" + Math.round((this.state.score/this.state.shotsTaken)*100)+ "%"
              }
              
    // })
    console.log("this works")
    console.log(this.state.percentage)
    return 
    }
render() {

    return (
        <div>
            <React.Fragment>
                <h2> {this.props.name}</h2>
                <img src={this.props.logo} />
                <button onClick={this.shots}>Take a shot!</button>
                <p>Shots Taken: {this.state.shotsTaken}</p>
                <p>score: {this.state.score}</p>
                { <span>{this.percentage()}</span> }
            </React.Fragment>
        </div>
    )
}
}


//Render the application
ReactDOM.render(
    <App />,
    document.getElementById('root')
);
